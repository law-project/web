import React, { useState, useEffect } from "react";
import "./App.css";
import API from "./tools/Api";
import LocalStorageFn from "./tools/LocalStorage";

function App() {
	const [user, setUser] = useState({});
	const [isLoading, setIsLoading] = useState(true);
	const [isAuthenticated, setIsAuthenticated] = useState(false);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	useEffect(() => {
		validate();
	}, []);

	function validate() {
		let auth = LocalStorageFn().get("auth");
		if (auth) {
			API.post("auth/validate/", null, {
				headers: {
					Authorization: "Bearer " + auth.token
				}
			})
				.then(res => {
					setIsAuthenticated(true);
					setUser(res.data.data);
					console.log(res.data.data);
				})
				.catch(err => {
					setIsAuthenticated(false);
					LocalStorageFn().remove("auth");
				})
				.then(() => {
					setIsLoading(false);
				});
		} else {
			setIsLoading(false);
			setIsAuthenticated(false);
		}
	}

	function login() {
		API.post("auth/login/", {
			email: email,
			password: password
		})
			.then(res => {
				LocalStorageFn().save("auth", res.data.data);
				validate();
			})
			.catch(err => {
				console.log(err.response.status);
				LocalStorageFn().remove("auth");
				setIsAuthenticated(false);
			})
			.then(() => {
				setIsLoading(false);
			});
	}

	function logout() {
		let auth = LocalStorageFn().get("auth");
		if (auth) {
			API.post("auth/logout/", null, {
				headers: {
					Authorization: "Bearer " + auth.token
				}
			})
				.then(res => {
					setIsAuthenticated(false);
					LocalStorageFn().remove("auth");
				})
				.catch(err => {
					console.log(err.response);
				})
				.then(() => {
					setIsLoading(false);
				});
		} else {
			setIsLoading(false);
		}
	}

	// function handleValidate() {
	// 	setIsLoading(true);
	// 	validate();
	// }

	function handleLogin() {
		setIsLoading(true);
		login();
	}

	function handleLogout() {
		setIsLoading(true);
		logout();
	}

	function handleEmailChange(e) {
		setEmail(e.target.value);
	}

	function handlePasswordChange(e) {
		setPassword(e.target.value);
	}

	return (
		<div className="App">
			<header className="App-header">
				{isLoading ? (
					<p>Loading...</p>
				) : isAuthenticated ? (
					<div>
						<p>Welcome {user.email}!</p>
						<br />
						<button onClick={handleLogout}>Logout</button>
					</div>
				) : (
					<div>
						<input
							type="email"
							onChange={handleEmailChange}
							placeholder="Email"
							value={email}
						/>
						<br />
						<input
							type="password"
							onChange={handlePasswordChange}
							placeholder="Password"
							value={password}
						/>
						<br />
						<button onClick={handleLogin}>Login</button>
					</div>
				)}
			</header>
		</div>
	);
}

export default App;
