module.exports = {
	apps: [
		{
			name: "Law Web",
			script: "server.js",
			error_file: "err.log",
			out_file: "out.log",
			log_file: "combined.log",
			time: true,
			combine_logs: true,
			env: {
				NODE_ENV: "staging",
				PORT: 3000
			},
			env_staging: {
				NODE_ENV: "staging",
				PORT: 3000
			},
			env_production: {
				NODE_ENV: "production",
				PORT: 3000
			}
		}
	]
};
