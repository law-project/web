const express = require("express");
const path = require("path");

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.static(path.resolve(__dirname, "..", "web/build")));
app.get("*", (request, response) => {
	response.sendFile(path.join(__dirname, "/build", "index.html"));
});

app.listen(PORT, () => {
	console.log(`Listening on port ${PORT}`);
});
